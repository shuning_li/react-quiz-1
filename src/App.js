import React from 'react';
import PlayingPanel from './containers/PlayingPanel';
import ControlPanel from './containers/ControlPanel';
import './App.less';

const ALPHABETIC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

class App extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      guessResult: new Array(4).fill(''),
      realResult: new Array(4).fill(''),
      correct: false,
      isGuessed: false,
      showResult: false
    };
    this.handleGuessResultChange = this.handleGuessResultChange.bind(this);
    this.checkResult = this.checkResult.bind(this);
    this.toggleRealResult = this.toggleRealResult.bind(this);
    this.generateRealResult = this.generateRealResult.bind(this);
  }

  handleGuessResultChange(e) {
    const result = e.target.value.split('');
    const guessResult = this.state.guessResult.map((item, index) => result[index] ? result[index] : '');
    this.setState({
      ...this.state,
      guessResult
    })
  }

  checkResult(e) {
    e.preventDefault();
    const { guessResult, realResult, showResult } = this.state;
    if (!guessResult.join('').match(/[A-Z]{4}/) || showResult) {
      return
    }
    const correct = guessResult.join('') === realResult.join('');
    this.setState({
      ...this.state,
      correct,
      isGuessed: true
    })
  }

  toggleRealResult(show, clear) {
    this.setState({
      showResult: show
    });
    if (clear) {
      this.setState({
        guessResult: new Array(4).fill(''),
        correct: false,
        isGuessed: false,
      })
    }
  }

  generateRealResult() {
    const realResult = [];
    while (realResult.length !== 4) {
      let index = Math.ceil(Math.random() * 25);
      realResult.push(ALPHABETIC[index]);
    }
    this.setState({
      guessResult: new Array(4).fill(''),
      correct: false,
      isGuessed: false,
      showResult: true,
      realResult
    }, () => {
      setTimeout(() => {
        this.setState({
          ...this.state,
          showResult: false
        })
      }, 3000)
    })
  }

  render() {
    const { guessResult, realResult, correct, isGuessed, showResult } = this.state;
    return (
      <main className="game-main">
        <PlayingPanel correct={correct}
                      isGuessed={isGuessed}
                      handleChange={this.handleGuessResultChange}
                      handleSubmit={this.checkResult}
                      guessResult={guessResult}/>
        <ControlPanel realResult={realResult}
                      showResult={showResult}
                      toggleRealResult={this.toggleRealResult}
                      handleGenerateRealResult={this.generateRealResult}/>
      </main>
    )
  }
}

export default App;