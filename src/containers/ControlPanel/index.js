import React from 'react';
import './style.less';
import GuessList from '../../components/GuessList';

const ControlPanel = props => {
  const { realResult, showResult, toggleRealResult, handleGenerateRealResult } = props;
  return (
    <section className="control-panel">
      <h2>New Card</h2>
      <section>
        <section>
          <button className="new-game-btn" onClick={handleGenerateRealResult}>New Card</button>
        </section>
        <GuessList list={showResult ? realResult : new Array(4).fill('')}/>
        <section className="show-result-btn-wrapper">
          <button className="show-result-btn" onClick={() => toggleRealResult(true, true)}>Show Result</button>
        </section>
      </section>
    </section>
  )
};

export default ControlPanel;