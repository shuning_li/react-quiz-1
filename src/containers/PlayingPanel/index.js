import React from 'react';
import GuessCard from './GuessCard';
import ResultCard from './ResultCard';
import './style.less';

const PlayingCard = props => {
  const { handleChange, guessResult, handleSubmit, isGuessed, correct } = props;
  return (
    <section className="playing-panel">
      <ResultCard guessResult={guessResult} isGuessed={isGuessed} correct={correct}/>
      <GuessCard handleChange={handleChange} handleSubmit={handleSubmit} guessResult={guessResult}/>
    </section>
  )
};

export default PlayingCard;