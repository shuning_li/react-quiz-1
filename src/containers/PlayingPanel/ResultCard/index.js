import React from 'react';
import './style.less';
import GuessList from '../../../components/GuessList';

const ResultCard = props => {
  const { guessResult, correct, isGuessed } = props;
  return (
    <section className="result-card">
      <h2>Your Result</h2>
      <GuessList list={guessResult}/>
      <section className="status">
        <span className={correct ? 'success' : 'failed'}>{isGuessed && (correct ? 'SUCCESS' : 'FAILED')}</span>
      </section>
    </section>
  )
};

export default ResultCard;