import React from 'react';
import './style.less';

const GuessCard = props => {
  const { guessResult, handleChange, handleSubmit } = props;
  return (
    <section className="guess-card">
      <h2>Guess Card</h2>
      <form className="guess-form" onSubmit={handleSubmit}>
        <section>
          <input onChange={handleChange} value={guessResult.join('')} size={4} type="text"/>
        </section>
        <button>Guess</button>
      </form>
    </section>
  )
};

export default GuessCard;