import React from 'react';
import './style.less';

const GuessList = props => {
  return (
    <section className="guess-list">
      {props.list.map((item, index) => (
        <input key={index} value={item} readOnly type="text"/>
      ))}
    </section>
  )
};

export default GuessList;